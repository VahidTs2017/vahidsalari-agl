﻿using System;
using System.Collections.Generic;
using AGLTest.Core;
using AGLTest.Models;
using AGLTest.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AglUnitTest
{
    [TestClass]
    public class PeopleValidationUnitTest
    {

        public IAglTestService AglTestService;
        public Mock<ISettings> SettingsMock = new Mock<ISettings>();
        public Mock<IWebDataServices> WebDataServicesMock = new Mock<IWebDataServices>();

        public void SetupMethods()
        {
            var testJsonUrl = @"http://agl-developer-test.azurewebsites.net/people.json";
            AglTestService = new AglTestService(SettingsMock.Object, WebDataServicesMock.Object);
            SettingsMock.Setup(s => s.WebServiceUrl).Returns(testJsonUrl);
            var testData = new List<People>();

        }

        [TestMethod]
        public void TestInvalidName()
        {
            SetupMethods();
            var testData = GetInvalidNameTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[0]));
        }

        [TestMethod]
        public void TestNullName()
        {
            SetupMethods();
            var testData = GetInvalidNameTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[1]));
        }

        [TestMethod]
        public void TestEmptyName()
        {
            SetupMethods();
            var testData = GetInvalidNameTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[2]));
        }

        [TestMethod]
        public void TestInvalidGender()
        {
            SetupMethods();
            var testData = GetInvalidGenderTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[0]));
        }

        [TestMethod]
        public void TestNullGenderType()
        {
            SetupMethods();
            var testData = GetInvalidGenderTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[1]));
        }

        [TestMethod]
        public void TestEmptyGender()
        {
            SetupMethods();
            var testData = GetInvalidGenderTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[2]));
        }

        [TestMethod]
        public void TestInvalidPetType()
        {
            SetupMethods();
            var testData = GetInvalidPetTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[0]));
        }

        [TestMethod]
        public void TestNullPet()
        {
            SetupMethods();
            var testData = GetInvalidPetTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[1]));
        }

        [TestMethod]
        public void TestEmptyPet()
        {
            SetupMethods();
            var testData = GetInvalidPetTestData();
            Assert.IsFalse(AglTestService.PeopleValidator(testData[2]));
        }





        [TestMethod]
        public void TestValidData()
        {
            SetupMethods();
            Assert.IsTrue(AglTestService.PeopleValidator(GetValidTestPeopleData()));

        }

        public People GetValidTestPeopleData()
        {
            return new People
            {
                Name = "Bob",
                Gender = "Male",
                Age = 52,
                Pets = new List<Pet>()
                {
                    new Pet
                    {
                        Name = "Garfield",
                        Type = "Cat"
                    },
                },
            };
        }

        public List<People> GetInvalidNameTestData()
        {
            return new List<People>()
            {
                new People
                {
                    Name = "Bob!@",
                    Gender = "Male",
                    Age = 100,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Garfield",
                            Type = "Cat"
                        },
                    },
                },
                new People
                {
                    Name = null,
                    Gender = "Female",
                    Age = -1,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Sam",
                            Type = "Fish"
                        },
                    },
                },
                new People
                {
                    Name = "",
                    Gender = "Female",
                    Age = 40,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        },
                    },
                }
            };
        }

        public List<People> GetInvalidGenderTestData()
        {
            return new List<People>()
            {
                new People
                {
                    Name = "Tania",
                    Gender = "",
                    Age = 40,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        },
                    },
                },
                new People
                {
                    Name = "Tania",
                    Gender = null,
                    Age = 40,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        },
                    },
                },
                new People
                {
                    Name = "Tania",
                    Gender = "M@le",
                    Age = 40,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        },
                    },
                }
            };
        }

        public List<People> GetInvalidAgeTestData()
        {
            return new List<People>()
            {
                new People
                {
                    Name = "Tania",
                    Gender = "Female",
                    Age = -1,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        },
                    },

                },
                new People
                {
                    Name = "Tania",
                    Gender = "Female",
                    Age = 101,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Dog"
                        }
                    }
                }
            };
        }



        public List<People> GetInvalidPetTestData()
        {
            return new List<People>()
            {

                new People
                {
                    Name = "Sara",
                    Gender = "Female",
                    Age = 10,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "S@m",
                            Type = "Puma"
                        },
                    },
                },
                new People
                {
                    Name = "Henry",
                    Gender = "Male",
                    Age = 22,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = null,
                            Type = "Fish"
                        },
                    },
                },
                new People
                {
                    Name = "John",
                    Gender = "Male",
                    Age = 47,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "",
                            Type = "Fish"
                        },
                    },
                }
            };
        }
    }
}
