﻿using System;
using System.Collections.Generic;
using AGLTest.Core;
using AGLTest.Enums;
using AGLTest.Models;
using AGLTest.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AglUnitTest
{
    [TestClass]
    public class PeopleUnitTest
    {

        public IAglTestService AglTestService;
        public Mock<ISettings> SettingsMock = new Mock<ISettings>();
        public Mock<IWebDataServices> WebDataServicesMock = new Mock<IWebDataServices>();

        public void SetupMethods()
        {
            var testJsonUrl = @"http://agl-developer-test.azurewebsites.net/people.json";
            AglTestService = new AglTestService(SettingsMock.Object, WebDataServicesMock.Object);
            SettingsMock.Setup(s => s.WebServiceUrl).Returns(testJsonUrl);
            var testData = new List<People>();
            WebDataServicesMock.Setup(s => s.ReadJsonData(It.IsAny<string>())).Returns(GetTestPeopleData());

        }

        [TestMethod]
        public void TestCreateCatList()
        {
            SetupMethods();
            var CatListForTest = AglTestService.CreateCatList();

            Assert.AreEqual(CatListForTest[0].OwnerGender, TestGroupData()[0].OwnerGender);
            Assert.AreEqual(CatListForTest[0].CatNames[0], TestGroupData()[0].CatNames[0]);
            Assert.AreEqual(CatListForTest[0].CatNames[1], TestGroupData()[0].CatNames[1]);
            Assert.AreEqual(CatListForTest[0].CatNames[2], TestGroupData()[0].CatNames[2]);
            Assert.AreEqual(CatListForTest[0].CatNames[3], TestGroupData()[0].CatNames[3]);

            Assert.AreEqual(CatListForTest[1].OwnerGender, TestGroupData()[1].OwnerGender);
            Assert.AreEqual(CatListForTest[1].CatNames[0], TestGroupData()[1].CatNames[0]);
            Assert.AreEqual(CatListForTest[1].CatNames[1], TestGroupData()[1].CatNames[1]);
            Assert.AreEqual(CatListForTest[1].CatNames[2], TestGroupData()[1].CatNames[2]);
            Assert.AreEqual(CatListForTest[1].CatNames[3], TestGroupData()[1].CatNames[3]);

        }

        public List<People> GetTestPeopleData()
        {
            return new List<People>()
            {
                new People
                {
                    Name = "Bob",
                    Gender = "Male",
                    Age = 23,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Garfield",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Fido",
                            Type = "Fish"
                        },
                        new Pet
                        {
                            Name = "Berny",
                            Type = "Dog"
                        },
                    }
                },
                new People
                {
                    Name = "John",
                    Gender = "Male",
                    Age = 37,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Max",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Fini",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Otto",
                            Type = "Cat"
                        },
                    }
                },
                new People
                {
                    Name = "Jennifer",
                    Gender = "Female",
                    Age = 25,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Tom",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Jerry",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Nemo",
                            Type = "Fish"
                        },
                    }
                },
                new People
                {
                    Name = "Mary",
                    Gender = "Female",
                    Age = 31,
                    Pets = new List<Pet>()
                    {
                        new Pet
                        {
                            Name = "Henry",
                            Type = "Cat"
                        },
                        new Pet
                        {
                            Name = "Jobra",
                            Type = "Dog"
                        },
                        new Pet
                        {
                            Name = "Timmy",
                            Type = "Cat"
                        },
                    }
                }
            };
        }

        public List<GroupedCats> TestGroupData()
        {
            return new List<GroupedCats>()
            {
                new GroupedCats
                {
                    CatNames = new List<string>()
                    {
                        "Fini",
                        "Garfield",
                        "Max",
                        "Otto"
                    },
                    OwnerGender = Genders.Male.ToString()
                },
                new GroupedCats
                {
                    CatNames = new List<string>()
                    {
                        "Henry",
                        "Jerry",
                        "Timmy",
                        "Tom",
                    },
                    OwnerGender = "Female"
                }
            };
        }
    }
}
