﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Models;
using FluentValidation;

namespace AGLTest.Validator
{
    public class PeopleValidator:AbstractValidator<People>
    {
        public PeopleValidator()
        {
            RuleFor(People => People.Name)
                .NotNull()
                .NotEmpty()
                .Matches(@"^[a-zA-Z]+$")
                .Length(1, 100);
    
            RuleFor(People => People.Gender)
                .NotNull()
                .NotEmpty()
                .Matches(@"^[a-zA-Z]+$")
                .Matches(@"\b(Male|Female)\b");

            
            RuleFor(People => People.Age)
                .NotNull()
                .NotEmpty()
                .GreaterThan(0)
                .LessThan(100);

            RuleFor(People => People.Pets).SetCollectionValidator(new PetValidator());
        }
    }
}