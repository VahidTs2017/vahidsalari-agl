﻿using AGLTest.Models;
using FluentValidation;

namespace AGLTest.Validator
{
    public class PetValidator:AbstractValidator<Pet>
    {
        public PetValidator()
        {            
            RuleFor(Pet => Pet.Name)
                .NotNull()
                .NotEmpty()
                .Matches(@"^[a-zA-Z]+$")
                .Length(1, 100);

            RuleFor(Pet => Pet.Type)
                .NotNull()
                .NotEmpty()
                .Length(1,50)
                .Matches(@"\b(Cat|Fish|Dog)\b");
        }
    }
}