﻿using AGLTest.Core;
using AGLTest.Models;
using System.Collections.Generic;
using System.Linq;
using AGLTest.Validator;
using log4net;
using AGLTest.Enums;

namespace AGLTest.Services
{
    public class AglTestService : IAglTestService
    {
        ISettings Settings;
        IWebDataServices WebDataServices;

        private static readonly ILog Log =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AglTestService(ISettings settings, IWebDataServices webDataServices)
        {
            Settings = settings;
            WebDataServices = webDataServices;
        }


        public List<GroupedCats> CreateCatList()
        {
            var peoples = WebDataServices.ReadJsonData(Settings.WebServiceUrl);
            if (peoples != null)
            {
                Log.Info("Data Read Successfully from WebService");
                foreach (var people in peoples)
                {
                    if (PeopleValidator(people))
                    {

                        var groupedCatsList = new List<GroupedCats>();
                        var allCats = peoples
                            .Where(p => p.Pets != null)
                            .Select(q => new {q.Gender, Pets = q.Pets.Where(p => p != null && p.Type == PetTypes.Cat.ToString()).Select(p => p.Name)});
                        var maleOwnerList = allCats.Where(g => g.Gender == Genders.Male.ToString()).Select(p => p.Pets).SelectMany(x => x).ToList();
                        maleOwnerList.Sort();
                        var femaleOwnerList = allCats.Where(x => x.Gender == Genders.Female.ToString()).Select(y => y.Pets).SelectMany(x => x).ToList();
                        femaleOwnerList.Sort();

                        if (maleOwnerList.Any())
                        {
                            groupedCatsList.Add(new GroupedCats
                            {
                                CatNames = maleOwnerList,
                                OwnerGender = Genders.Male.ToString()
                            });

                        }
                        if (femaleOwnerList.Any())
                        {
                            groupedCatsList.Add(new GroupedCats
                            {
                                CatNames = femaleOwnerList,
                                OwnerGender = Genders.Female.ToString()
                            });
                        }
                        return groupedCatsList;
                    }

                }
            }
            return null;
        }

        public bool PeopleValidator(People people)
        {
            var peopleValidator = new PeopleValidator();
            var validationResult = peopleValidator.Validate(people);
            if (validationResult.IsValid)
            {
                return true;
            }
            var data = validationResult.Errors.Select(x => x.ErrorMessage).ToList().Distinct();
            Log.Error("People object Validation Error:" + data.Aggregate(";", (current, errMsg) => current + errMsg));
            return false;
        }
    }

    public interface IAglTestService
    {
        List<GroupedCats> CreateCatList();
        bool PeopleValidator(People people);
    }
}