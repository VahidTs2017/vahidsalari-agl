﻿using AGLTest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGLTest.Controllers
{
    public class HomeController : Controller
    {
        IAglTestService AglTestService;
        public HomeController(IAglTestService aglTestService)
        {
            AglTestService = aglTestService;
        }
        public ActionResult Index()
        {
            if (ModelState.IsValid)
            {
                var catlist = AglTestService.CreateCatList();
                if (catlist != null)
                    return View(catlist);
            }
            return View();
        }
    }
}