﻿using System.Collections.Generic;

namespace AGLTest.Models
{
    public class People
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        public List<Pet> Pets;
    }
    public class Pet
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public class CatNameComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                if (x == null)
                {
                    if (y == null)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    if (y == null)
                    {
                        return 1;
                    }
                    else
                    {
                        int retval = x.Length.CompareTo(y.Length);
                        if (retval != 0)
                        {
                            return retval;
                        }
                        else
                        {
                            return x.CompareTo(y);
                        }
                    }
                }

            }
        }
    }
}