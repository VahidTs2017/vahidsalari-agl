﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
namespace AGLTest.Core
{
    public class Settings : ISettings
    {
        public string WebServiceUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["WebServiceUrl"].ToString();
            }
        }
    }

    public interface ISettings
    {
        string WebServiceUrl { get;  }
    }
}